# DevOps Final Fallback
### Task
Classifying Email as Spam or Non-Spam

This repository has the fallback solution. Used when the proper implementation of a solution does not work.
The problem is solved using naive bayess.
